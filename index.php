<?php session_start();?>
<?php include("partial/header.php"); ?>
<?php include("./admin/koneksi.php"); ?>

<div id="layoutSidenav_content">

    <main>

        <div class="container-fluid">

            <div id="carousel-dashboard" class="carousel slide mb-5" data-ride="carousel">
                <ul class="carousel-indicators">
                    <li data-target="#carousel-dashboard" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-dashboard" data-slide-to="1"></li>
                </ul>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" style="filter:blur(7px); height: 600px; object-fit:cover"
                            src="./admin/assets/img/metamask-web-1200x900-1.jpg" alt="Los Angeles">
                        <div class="carousel-caption" style="transform: translateY(-165%);">
                            <h1 class="text-dark">Selamat Datang di META MARKET</h1>
                            <h5 class="text-dark">Anda dapat membeli barang dari gudang kami secara online</h5>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" style="filter:blur(7px); height: 600px; object-fit:cover"
                            src="./admin/assets/img/Empty-Warehouse.jpg" alt="Chicago">
                        <div class="carousel-caption" style="transform: translateY(-165%);">
                            <h1 class="text-dark">Berbagai macam barang tersedia di sini</h1>
                            <h5 class="text-dark">Anda tinggal mencari barang yang ingin anda inginkan melalu device anda</h5>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carousel-dashboard" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#carousel-dashboard" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>
            </div>

            <h1>Yang ada di dalam gudang kami</h1>

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Nama Barang</th>
                                <th>Stok</th>
                                <th>Kategori</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            $ambilsemuadatastock = mysqli_query($con, "select * from stock where jumlahbarang > 0");
                            $i = 1;
                            while ($data = mysqli_fetch_array($ambilsemuadatastock)) {
                                $idb = $data['idbarang'];
                                $namabarang = $data['namabarang'];
                                $stock = $data['jumlahbarang'];
                                $kategori = $data['kategori'];
                            ?>
                            <tr>
                                <td><?= $namabarang; ?></td>
                                <td><?= $stock; ?></td>
                                <td><?= $kategori; ?></td>
                                <td>
                                    <button type="button" class="btn btn-primary" data-toggle="modal"
                                        data-target="#belistock<?=$idb;?>">
                                        Beli
                                    </button>
                                </td>
                            </tr>

                            <div class="modal fade" id="belistock<?= $idb; ?>">

                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <!-- Modal Header -->
                                        <div class="modal-header">
                                            <h4 class="modal-title">Barang Yang Dibeli</h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>

                                        <!-- Modal body -->
                                        <form action="./admin/function.php" method="post">
                                            <div class="modal-body">
                                                <select name="barangnya" class="form-control">
                                                    <option value="<?= $idb; ?>"><?= $namabarang; ?>
                                                    </option>
                                                </select>
                                                <br>
                                                <input type="number" name="qty" placeholder="Quantity"
                                                    class="form-control" required>
                                                <br>
                                                <input style="display: none;" type="teks" name="penerima"
                                                    placeholder="Penerima" class="form-control" value="<?= $pesan;?>"
                                                    required>
                                                <br>
                                                <?php
                                                if (isset($_SESSION['username'])) {
                                                echo '<button type="submit" class="btn btn-primary"
                                                name="belibarang">Submit</button>';
                                                }
                                                else {
                                                echo "<p class='text-danger'> Untuk membeli barang, anda harus login terlebih dahulu </p> <br>";
                                                echo "<a type='button' class='btn btn-danger' href='./login.php'>
                                                    Login
                                                </a>";
                                                }
                                                ?>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>

                            <?php
                            };

                            ?>

                        </tbody>

                    </table>
                </div>

            </div>

    </main>

    <?php require("./partial/footer.php"); ?>