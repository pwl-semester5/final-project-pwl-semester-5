-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Waktu pembuatan: 16 Jan 2022 pada 09.22
-- Versi server: 5.7.33
-- Versi PHP: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbuaspwl`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `keluar`
--

CREATE TABLE `keluar` (
  `idkeluar` int(11) NOT NULL,
  `idbarang` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `penerima` varchar(20) NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `keluar`
--

INSERT INTO `keluar` (`idkeluar`, `idbarang`, `tanggal`, `penerima`, `qty`) VALUES
(1, 3, '2022-01-04 16:13:54', 'Ilham', 10),
(2, 1, '2022-01-05 15:50:26', 'Iqbal', 5),
(3, 1, '2022-01-05 17:39:17', 'AHHA', 7),
(4, 9, '2022-01-06 07:49:14', 'AHHA', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `masuk`
--

CREATE TABLE `masuk` (
  `idmasuk` int(11) NOT NULL,
  `idbarang` int(11) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `keterangan` varchar(20) NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `masuk`
--

INSERT INTO `masuk` (`idmasuk`, `idbarang`, `tanggal`, `keterangan`, `qty`) VALUES
(1, 1, '2022-01-04 15:37:35', 'Iqbal', 5),
(2, 1, '2022-01-04 15:45:33', 'Ilham', 10),
(3, 3, '2022-01-04 15:46:29', 'Iqbal', 5),
(4, 1, '2022-01-04 16:09:51', 'AHHA', 5),
(5, 1, '2022-01-04 16:10:12', 'Iqbal', 5),
(6, 5, '2022-01-05 15:25:04', 'Iqbal', 10),
(7, 1, '2022-01-05 17:24:57', 'Ilham', 5),
(8, 1, '2022-01-05 17:26:17', 'hihi', 5),
(9, 1, '2022-01-05 17:28:48', 'AHHAy', 5),
(10, 9, '2022-01-05 17:31:18', 'Iqbal', 100),
(11, 9, '2022-01-05 17:33:08', 'Ilham', 10),
(12, 1, '2022-01-05 17:34:10', 'AHHA', 15),
(13, 1, '2022-01-05 17:35:00', 'Iqbal', 2),
(14, 10, '2022-01-05 17:44:08', 'Ilham', 4),
(15, 11, '2022-01-06 07:16:53', 'Iqbal', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `register`
--

CREATE TABLE `register` (
  `id_user` int(11) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `pass` varchar(20) NOT NULL,
  `level` enum('user','admin') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `register`
--

INSERT INTO `register` (`id_user`, `firstname`, `lastname`, `email`, `pass`, `level`) VALUES
(33, 'Admin', '', 'admin@gmail.com', 'admin', 'admin'),
(35, 'User', 'Dwi', 'user@gmail.com', 'user', 'user');

-- --------------------------------------------------------

--
-- Struktur dari tabel `stock`
--

CREATE TABLE `stock` (
  `idbarang` int(11) NOT NULL,
  `namabarang` varchar(30) NOT NULL,
  `jumlahbarang` int(11) NOT NULL,
  `kategori` varchar(20) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `stock`
--

INSERT INTO `stock` (`idbarang`, `namabarang`, `jumlahbarang`, `kategori`, `tanggal`) VALUES
(1, 'iPhone', 30, 'Smartphone', '2022-01-04 14:03:31'),
(3, 'Asus ROG', 10, 'Laptop', '2022-01-04 14:44:29'),
(6, 'Kulkas', 5, 'Elektronik', '2022-01-05 15:28:58'),
(7, 'Samsung', 5, 'Smartphone', '2022-01-05 16:20:26'),
(8, 'Xiaomi', 30, 'Smartphone', '2022-01-05 17:11:26'),
(9, 'OPPO Reno 5', 95, 'Smartphone', '2022-01-05 17:31:00'),
(10, 'PC', 5, 'Elektronik', '2022-01-05 17:41:13'),
(11, 'Speaker Tv', 5, 'Elektronik', '2022-01-06 07:16:42');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `keluar`
--
ALTER TABLE `keluar`
  ADD PRIMARY KEY (`idkeluar`);

--
-- Indeks untuk tabel `masuk`
--
ALTER TABLE `masuk`
  ADD PRIMARY KEY (`idmasuk`);

--
-- Indeks untuk tabel `register`
--
ALTER TABLE `register`
  ADD PRIMARY KEY (`id_user`);

--
-- Indeks untuk tabel `stock`
--
ALTER TABLE `stock`
  ADD PRIMARY KEY (`idbarang`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `keluar`
--
ALTER TABLE `keluar`
  MODIFY `idkeluar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `masuk`
--
ALTER TABLE `masuk`
  MODIFY `idmasuk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT untuk tabel `register`
--
ALTER TABLE `register`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT untuk tabel `stock`
--
ALTER TABLE `stock`
  MODIFY `idbarang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
