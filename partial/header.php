<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Meta Market</title>
    <link href="./admin/css/styles.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet"
        crossorigin="anonymous" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" crossorigin="anonymous">
    </script>
</head>

<body class="sb-nav-fixed">
    <nav class="sb-topnav navbar navbar-expand navbar-dark bg-secondary d-flex justify-content-between">
        <a class="navbar-brand" href="index.php">META MARKET</a>

        <div class="d-flex flex-row">
            <a href="index.php" class="p-2 text-white">Home</a>
            <a href="about.php" class="p-2 text-white">About</a>
            <a href="contact.php" class="p-2 text-white">Contact Us</a>
            <ul class="navbar-nav ml-auto ml-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false"><i class="fas fa-user fa-fw"></i></a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                        <a class="dropdown-item" href="login.php"><i class="fas fa-user"></i>
                            <?php
    
                            if (isset($_SESSION['username'])) {
                                $pesan =  htmlspecialchars($_SESSION['username']);
                                //unset($_SESSION['username']);
                                echo $pesan;
                            }
                            else{
                                echo "login";
                            }
                            ?></a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="logout.php"><i class="fas fa-sign-out-alt"></i> Logout</a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>

    <div style="margin:100px 0 0 0;">