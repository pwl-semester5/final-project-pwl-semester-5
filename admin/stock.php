<?php session_start();
include("ceklogin.php"); ?>
<?php include("partial/header.php"); ?>
<?php include("koneksi.php"); ?>

<div id="layoutSidenav_content">
  <main>
    <div class="container-fluid">
      <h1 class="mt-4">Stock Barang</h1>
      <ol class="breadcrumb mb-4">
        <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
        <li class="breadcrumb-item active">Stock Barang</li>
      </ol>
      <div class="card mb-4">
        <div class="card-header">
          <!-- Button to Open the Modal -->
          <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
            Tambah Stock
          </button>
        </div>
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Nama Barang</th>
                  <th>Jumlah Barang</th>
                  <th>Kategori</th>
                  <th>Tanggal</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>
                <?php

                $ambilsemuadatastock = mysqli_query($con, "select * from stock");
                $i = 1;
                while ($data = mysqli_fetch_array($ambilsemuadatastock)) {

                  $namabarang = $data['namabarang'];
                  $stock = $data['jumlahbarang'];
                  $kategori = $data['kategori'];
                  $tanggal = $data['tanggal'];
                  $idb = $data['idbarang'];


                ?>



                  <tr>
                    <td><?= $i++; ?></td>
                    <td><?= $namabarang; ?></td>
                    <td><?= $stock; ?></td>
                    <td><?= $kategori; ?></td>
                    <td><?= $tanggal; ?></td>
                    <td>
                      <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#editstock<?= $idb; ?>">
                        Edit
                      </button>
                      <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deletestock<?= $idb; ?>">
                        Delete
                      </button>
                    </td>
                  </tr>

                  <!-- Edit Modal -->
                  <div class="modal fade" id="editstock<?= $idb; ?>">
                    <div class="modal-dialog">
                      <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                          <h4 class="modal-title">Edit Barang</h4>
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <form action="function.php" method="post">
                          <div class="modal-body">
                            <input type="text" name="namabarang" value="<?= $namabarang; ?>" class="form-control" required>
                            <br>
                            <input type="text" name="jumlahbarang" value="<?= $stock; ?>" class="form-control" required>
                            <br>
                            <input type="text" name="kategori" value="<?= $kategori; ?>" class="form-control" required>
                            <br>
                            <input type="hidden" name="idb" value="<?= $idb; ?>">
                            <button type="submit" class="btn btn-primary" name="updatebarang">Update</button>

                          </div>
                        </form>

                      </div>
                    </div>
                  </div>


                  <!-- Delete Modal -->
                  <div class="modal fade" id="deletestock<?= $idb; ?>">
                    <div class="modal-dialog">
                      <div class="modal-content">

                        <!-- Modal Header -->
                        <div class="modal-header">
                          <h4 class="modal-title">Hapus Barang ?</h4>
                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <form action="function.php" method="post">
                          <div class="modal-body">
                            Apakah kamu yakin ingin menghapus <?= $namabarang; ?> ?
                            <input type="hidden" name="idb" value="<?= $idb; ?>">
                            <br>
                            <br>
                            <button type="submit" class="btn btn-danger" name="hapusbarang">Hapus</button>

                          </div>
                        </form>

                      </div>
                    </div>
                  </div>


                <?php
                };

                ?>

              </tbody>

            </table>
          </div>
        </div>
      </div>
    </div>
  </main>


  <?php include("partial/footer.php"); ?>

  <!-- The Modal -->
  <div class="modal fade" id="myModal">
    <div class="modal-dialog">
      <div class="modal-content">

        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title">Tambah Barang</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <!-- Modal body -->
        <form action="function.php" method="post">
          <div class="modal-body">
            <input type="text" name="namabarang" placeholder="Nama Barang" class="form-control" required>
            <br>
            <input type="number" name="jumlahbarang" placeholder="Stok" class="form-control" required>
            <br>
            <input type="text" name="kategori" placeholder="Kategori Barang" class="form-control" required>
            <br>
            <button type="submit" class="btn btn-primary" name="addnewbarang">Submit</button>

          </div>
        </form>

      </div>
    </div>
  </div>