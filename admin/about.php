<?php session_start();
include("ceklogin.php"); ?>
<?php include("partial/header.php"); ?>
<?php include("koneksi.php"); ?>


<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">About Us</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                <li class="breadcrumb-item active">About Us</li>
            </ol>


            <div class="about">
                <div class="about-img"></div>
                <div class="about-think">
                    <h3>Meta Market</h3>
                    <p>Selamat datang di Meta Market sumber nomor satu untuk kebutuhan rumah tangga Anda. Kami menyediakan barang-barang rumah tangga,
                        elektronik dan barang-barang keseharian lainnya yang anda butuhkan. Kami berdedikasi untuk memberikan Anda produk dengan kualitas
                        yang terbaik. Aplikasi ini cocok untuk anda yang sulit keluar rumah namun harus berbelanja keperluan yang anda butuhkan.
                    <p>
                        Dengan adanya aplikasi ini, kami berharap dapat membantu Anda dalam mencari barang-barang yang anda butuhkan saat ini.
                        Jika Anda memiliki pertanyaan jangan ragu untuk menghubungi kami.

                    </p>
                    </p>
                </div>
            </div>

            <div class="aboutvm">
                <div class="aboutvm-visi">
                    <h3>Visi</h3>
                    <ul>
                        <li>Menjadikan aplikasi store terbaik </li>
                        <li>Mendahulukan kepuasan konsumen dengan menjual produk yang memiliki kualitas terbaik</li>
                    </ul>
                </div>
                <div class="aboutvm-misi">
                    <h3>Misi</h3>
                    <ul>
                        <li>Memberikan pelayanan yang terbaik untuk konsumen</li>
                        <li>Memudahkan konsumen dalam memilih produk dengan cara mencantumkan harga </li>
                        <li>Meminimalisir waktu tunggu untuk konsumen dengan cara melayani dalam waktu yang lebih cepat </li>
                        <li>Memberikan kemudahan dalam penggunaan aplikasi</li>
                    </ul>
                </div>
            </div>
        </div>
    </main>


    <?php include("partial/footer.php"); ?>