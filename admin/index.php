<?php session_start();
include("ceklogin.php"); ?>
<?php include("partial/header.php"); ?>
<?php include("koneksi.php"); ?>

<div id="layoutSidenav_content">

    <main>

        <div class="container-fluid">
            <div class="jumbotron bg-secondary">
                <h1 class="text-white display-4">Dashboard</h1>
                <h3 class="text-white ">Selamat Datang <?= $_SESSION['username'] ?></h3>
            </div>

            <div id="carousel-dashboard" class="carousel slide mb-5" data-ride="carousel">
                <ul class="carousel-indicators">
                    <li data-target="#carousel-dashboard" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-dashboard" data-slide-to="1"></li>
                </ul>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img class="d-block w-100" style="filter:blur(7px); height: 600px; object-fit:cover" src="./assets/img/warehouse-management.jpg" alt="Los Angeles">
                        <div class="carousel-caption" style="transform: translateY(-165%);">
                            <h1 class="text-dark" >Kelola Barang Gudang Anda</h1>
                            <h5 class="text-dark">Kelola Barang sekarang dapat dilakukan secara online</h5>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" style="filter:blur(7px); height: 600px; object-fit:cover" src="./assets/img/Business_Manager.jpg" alt="Chicago">
                        <div class="carousel-caption" style="transform: translateY(-165%);">
                            <h1 class="text-dark">Kelola Adminitrator Gudang Anda</h1>
                            <h5 class="text-dark">Kelola Administrator yang berhak mengelola barang gudang anda</h5>
                        </div>
                    </div>
                </div>
                <a class="carousel-control-prev" href="#carousel-dashboard" data-slide="prev">
                    <span class="carousel-control-prev-icon"></span>
                </a>
                <a class="carousel-control-next" href="#carousel-dashboard" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
                </a>
            </div>

            <h1>Yang bisa anda lakukan</h1>

            <div class="card-deck mb-5 ">

                <div class="card">
                    <img src="./assets/img/warehouse-management.jpg" alt="" class="card-img-top">
                    <div class="card-body">
                        <h4 class="card-title text-black">Kelola Barang</h4>
                        <p class="card-text text-black">Kelola Barang Gudang Anda</p>
                    </div>
                </div>

                <div class="card">
                    <img src="./assets/img/Business_Manager.jpg" alt="" class="card-img-top">
                    <div class="card-body">
                        <h4 class="card-title text-black">Kelola Admin</h4>
                        <p class="card-text text-black">Kelola Administrator</p>
                    </div>
                </div>

            </div>

        </div>

    </main>

    <?php require("./partial/footer.php"); ?>