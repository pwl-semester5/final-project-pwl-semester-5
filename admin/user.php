<?php session_start();
include("koneksi.php");
require("ceklogin.php") ?>

<?php include("partial/header.php"); ?>
<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Kelola User</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                <li class="breadcrumb-item active">Kelola User</li>
            </ol>
            <div class="card mb-4">

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead class="thead-dark">
                                <tr>
                                    <th>ID</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php
                                $sql = "SELECT id_user,firstname,lastname,email FROM register WHERE level = 'user' ORDER BY id_user ASC";
                                $hasil = mysqli_query($con, $sql) or exit("Error query: <b>" . $sql . "</b>.");
                                $i = 1;
                                while ($data = mysqli_fetch_assoc($hasil)) {
                                    $a = $i++;
                                    echo "
                                        <tr>
                                            <td>$a</td>
                                            <td>$data[firstname] $data[lastname]</td>
                                            <td>$data[email]</td>
                                            <td><a href='?hapus=$data[id_user]' onClick=\"return confirm('Yakin menghapus data $data[firstname] $data[lastname]');\" >
                                            <input type='button' value='Hapus' class='btn btn-danger'></a></td>
                                        </tr>";
                                }
                                ?>
                            </tbody>

                            <?php
                            if (isset($_GET['hapus'])) {

                                mysqli_query($con, "delete from register where user_id='$_GET[hapus]'")
                                    or die(mysqli_error($con));

                                echo '<script type ="text/JavaScript">';
                                echo 'alert("Data berhasil dihapus")';
                                echo '</script>';
                                echo "<meta http-equiv=refresh content=0;URL='user.php'>";
                            }
                            ?>

                        </table>
                    </div>
                </div>
            </div>
        </div>

    </main>
    <?php include("partial/footer.php"); ?>