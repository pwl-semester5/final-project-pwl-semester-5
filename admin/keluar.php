<?php session_start();
include("ceklogin.php"); ?>
<?php include("partial/header.php"); ?>
<?php include("koneksi.php"); ?>

<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Barang Keluar</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="index.php">Dashboard</a></li>
                <li class="breadcrumb-item active">Barang Keluar</li>
            </ol>
            <div class="card mb-4">
                <div class="card-header">
                    <!-- Button to Open the Modal -->
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                        Barang Keluar
                    </button>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>Tanggal Keluar</th>
                                    <th>Nama Barang</th>
                                    <th>Jumlah Barang</th>
                                    <th>Keterangan</th>

                                </tr>
                            </thead>
                            <tbody>
                                <?php

                                $ambilsemuadatastock = mysqli_query($con, "select k.tanggal,s.namabarang,k.qty,k.penerima from keluar k, stock s where s.idbarang = k.idbarang");
                                while ($data = mysqli_fetch_assoc($ambilsemuadatastock)) {
                                    echo "
                    <tr>
                        <td>$data[tanggal]</td>
                        <td>$data[namabarang] </td>
                        <td>$data[qty]</td>
                        <td>$data[penerima]</td>
                        
                        
                    </tr>";
                                };

                                ?>

                            </tbody>


                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>


    <?php include("partial/footer.php"); ?>

    <!-- The Modal -->
    <div class="modal fade" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Barang Yang Keluar</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <form action="function.php" method="post">
                    <div class="modal-body">
                        <select name="barangnya" class="form-control">
                            <?php
                            $ambilsemuadatanya = mysqli_query($con, "select * from stock");
                            while ($fetcharray = mysqli_fetch_array($ambilsemuadatanya)) {
                                $namabarangnya = $fetcharray['namabarang'];
                                $idbarangnya = $fetcharray['idbarang'];
                            ?>

                                <option value="<?= $idbarangnya; ?>"><?= $namabarangnya; ?> </option>
                            <?php
                            }
                            ?>
                        </select>
                        <br>
                        <input type="number" name="qty" placeholder="Quantity" class="form-control" required>
                        <br>
                        <input type="teks" name="penerima" placeholder="Penerima" class="form-control" required>
                        <br>
                        <button type="submit" class="btn btn-primary" name="addbarangkeluar">Submit</button>

                    </div>
                </form>

            </div>
        </div>
    </div>