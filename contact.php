<?php session_start();
//include("ceklogin.php"); ?>
<?php include("partial/header.php"); ?>
<?php include("koneksi.php"); ?>


<div id="layoutSidenav_content">
    <main>
        <div class="container-fluid">
            <h1 class="mt-4">Contact Us</h1>
            <ol class="breadcrumb mb-4">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active">Contact Us</li>
            </ol>


            <div class="card mb-4">
                <div class="card-body">
                    Feel like contacting us? Submit your queries here and we will get back to you as soon as possible.
                </div>
                <div class="container2">
                    <div class="contactInfo">
                        <div class="box">
                            <div class="icon"><i class="fa fa-map-marker" aria-hidden="true"></i></div>
                            <div class="text">
                                <h2>Address</h2>
                                <p>Jl. Ring Road Utara, Ngringin, <br>Kec. Depok, Kabupaten Sleman,<br>Daerah
                                    Istimewa Yogyakarta,<br>55281</p>
                            </div>
                        </div>
                        <div class="box">
                            <div class="icon"><i class="fa fa-phone" aria-hidden="true"></i></div>
                            <div class="text">
                                <h2>Phone</h2>
                                <p>(0274) 884201</p>
                            </div>
                        </div>
                        <div class="box">
                            <div class="icon"><i class="fa fa-envelope" aria-hidden="true"></i></div>
                            <div class="text">
                                <h2>Email</h2>
                                <p>kelontongunyu452@gamil.com</p>
                            </div>
                        </div>
                    </div>
                    <div class="contactForm">
                        <form>
                            <h3>Send Message</h3>
                            <div class="inputBox">
                                <input type="text" name="" required="required">
                                <span>Full Name</span>
                            </div>
                            <div class="inputBox">
                                <input type="text" name="" required="required">
                                <span>Email</span>
                            </div>
                            <div class="inputBox">
                                <textarea required="required"></textarea>
                                <span>Type your Message...</span>
                            </div>
                            <div class="inputBox">
                                <input type="submit" name="" value="Send">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </main>


    <?php include("partial/footer.php"); ?>